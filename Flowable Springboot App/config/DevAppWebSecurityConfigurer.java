package dk.magenta.osflow.decision_engine.config;

import dk.magenta.osflow.decision_engine.utils.UtilMethods;
import org.flowable.rest.security.SecurityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * This was added because of issues encountered when POST-ing to the REST endpoints.
 * It disables CSRF and CORS protection.
 */
@Profile("dev")
@Order(710)
@Configuration
@EnableWebSecurity
public class DevAppWebSecurityConfigurer extends WebSecurityConfigurerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(DevAppWebSecurityConfigurer.class);

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        UtilMethods.logInfoMsg(logger, "Initialising Development web security config.........");
        http.cors().and().csrf().disable().authorizeRequests().anyRequest().permitAll();
    }
}
