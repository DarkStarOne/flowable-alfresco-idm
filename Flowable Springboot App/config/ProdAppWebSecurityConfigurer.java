package dk.magenta.osflow.decision_engine.config;

import dk.magenta.osflow.decision_engine.utils.UtilMethods;
import org.flowable.rest.security.BasicAuthenticationProvider;
import org.flowable.rest.security.SecurityConstants;
import org.flowable.spring.boot.FlowableSecurityAutoConfiguration;
import org.flowable.spring.boot.condition.ConditionalOnIdmEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Profile("alfresco")
@Order(710)
@Configuration
@EnableWebSecurity
@AutoConfigureAfter({FlowableSecurityAutoConfiguration.class})
@ConditionalOnIdmEngine
@ConditionalOnProperty(prefix = "flowable.idm.alfresco", name = "enabled", havingValue = "true", matchIfMissing = false)
public class ProdAppWebSecurityConfigurer extends WebSecurityConfigurerAdapter {
    private final static Logger logger = LoggerFactory.getLogger(ProdAppWebSecurityConfigurer.class);

    @Bean
    public AuthenticationProvider authenticationProvider() {
        BasicAuthenticationProvider basicAuthenticationProvider = new BasicAuthenticationProvider();
        return basicAuthenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        logger.info( "Initialising production web security config.........");
        // Securing endpoints with Spring security whilst permitting the health endpoint to be accessible by all.
        // See - https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-endpoints-security
        http.authenticationProvider(authenticationProvider())
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/actuator/health").permitAll()
                .antMatchers("/process-api/**", "/form-api/**", "/osflow-api/**")
                .hasAuthority(SecurityConstants.PRIVILEGE_ACCESS_REST_API)
                .anyRequest().authenticated()
                .and().httpBasic();

    }

}
