package dk.magenta.flowable.idm.alfresco.engine;

import java.net.HttpCookie;

public class AlfSession {
    private String sessionId, ticket, userName;
    private HttpCookie cookie;

    public AlfSession(String ticket, String userName) {
        this.ticket = ticket.replace("\"", "");
        this.userName = userName;
    }
    public AlfSession(String ticket, HttpCookie cookie) {
        this.ticket = ticket.replace("\"", "");
        this.cookie = cookie;
    }
    public AlfSession(String ticket, String userName, HttpCookie cookie) {
        this.ticket = ticket.replace("\"", "");
        this.userName = userName;
        this.cookie = cookie;
    }

    //<editor-fold desc="Gettters and Setters">
    public String getSessionId() {
        return this.cookie.getValue();
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public HttpCookie getCookie() {
        return cookie;
    }

    /**
     * We only want the session id cookie
     *
     * @param cookie
     */
    public void setCookie(HttpCookie cookie) {
        if (cookie.getName().equalsIgnoreCase(AlfrescoConnectionUtil.JSESSION_COOKIE_ID))
            this.cookie = cookie;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    //</editor-fold>

}
