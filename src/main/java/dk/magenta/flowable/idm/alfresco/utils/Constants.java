package dk.magenta.flowable.idm.alfresco.utils;

import org.springframework.stereotype.Component;

/**
 * @author DarkStar1.
 */
@Component
public class Constants {

    public static class HTTP_HEADERS {
        public static final String ACCEPT = "Accept";
        public static final String ACCEPT_CHARSET = "Accept-Charset";
        public static final String ACCEPT_ENCODING = "Accept-Encoding";
        public static final String ACCEPT_LANGUAGE = "Accept-Language";
        public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
        public static final String AUTHORIZATION = "Authorization";
        public static final String COOKIE = "COOKIE";
        public static final String CONTENT_ENCODING = "Content-Encoding";
        public static final String CONTENT_LANGUAGE = "Content-Language";
        public static final String CONTENT_LENGTH = "Content-Length";
        public static final String CONTENT_LOCATION = "Content-Location";
        public static final String CONTENT_TYPE = "Content-Type";
    }

    public static class MEDIA_MIME_TYPES{
        public static final String APPLICATION_JSON = "application/json";
        public static final String TEXT_XML = "text/xml";
        // public static final String = "";
    }

    /**
     * Literal strings (Note some may have plural versions)
     */
    public static final String STR_ADMIN = "admin";
    public static final String STR_CONTENT = "content";
    public static final String STR_ENTRY = "entry";
    public static final String STR_ENTRIES = "entries";
    public static final String STR_GROUP = "group";
    public static final String STR_HTTP = "http";
    public static final String STR_HTTPS = "https";
    public static final String STR_ID = "id";
    public static final String STR_LIST = "list";
    public static final String STR_MEMBERS = "members";
    public static final String STR_MEMBER_TYPE = "memberType";
    public static final String MIME_TYPE = "mimeType";
    public static final String STR_PERSON = "person";
    public static final String STR_POST = "POST";
    // public static final String STR_ = "";

}
