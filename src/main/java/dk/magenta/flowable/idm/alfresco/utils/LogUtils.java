package dk.magenta.flowable.idm.alfresco.utils;

import org.slf4j.Logger;

/**
 * @author DarkStar1.
 */
public class LogUtils {

    private static LogUtils instance = null;

    private LogUtils(){ }

    //Lazy initialisation
    public static LogUtils getInstance(){
        if(instance==null)
            instance = new LogUtils();
        return instance;
    }

    /**
     * Print a debug message in the logs
     *
     * @param logger
     * @param message the message to be printed
     */
    public static void logDebugMsg(Logger logger, String message, Object... args) {
        logger.debug("\n********************************************* DEBUG *********************************************\n"
                + message, args);
        logger.debug("\n*************************************************************************************************\n");
    }

    /**
     * Print a warning message in the logs.
     *
     * @param logger
     * @param message the message to be printed
     */
    public static void logWarning(Logger logger, String message, Object... args) {
        logger.warn("\n********************************************* WARNING *********************************************\n"
                + message, args);
        logger.warn("\n*************************************************************************************************\n");
    }

    /**
     * An error message that also prints the stack trace
     *
     * @param logger
     * @param msgObject
     * @param printStackTrace
     */
    public static void logError(Logger logger, String message, Throwable msgObject, boolean printStackTrace, Object... args) {
        logger.error("\n********************************************* ERROR *********************************************\n");
        logger.error( message + ":" + msgObject.getMessage(), args);
        if (printStackTrace)
            msgObject.printStackTrace();
        logger.error("\n*************************************************************************************************\n");
    }

    /**
     * Print a information message in the logs
     *
     * @param logger
     * @param message the message to be printed
     */
    public static void logInfoMsg(Logger logger, String message, Object... args) {
        logger.info("\n********************************************* INFORMATION *********************************************\n"
                + message, args);
        logger.info("\n*************************************************************************************************\n");
    }
}
