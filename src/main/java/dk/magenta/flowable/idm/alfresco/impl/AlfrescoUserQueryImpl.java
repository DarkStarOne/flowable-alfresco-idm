/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.magenta.flowable.idm.alfresco.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil;
import dk.magenta.flowable.idm.alfresco.utils.LogUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.idm.api.User;
import org.flowable.idm.engine.impl.UserQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AlfrescoUserQueryImpl extends UserQueryImpl {

    private static final long serialVersionUID = -1300719700229604375L;

    private static final Logger logger = LoggerFactory.getLogger(AlfrescoUserQueryImpl.class);

    protected AlfrescoConnectionUtil connectionUtil;

    public AlfrescoUserQueryImpl(AlfrescoConnectionUtil alfrescoConnectionUtil) {
        this.connectionUtil = alfrescoConnectionUtil;
    }

    @Override
    public long executeCount(CommandContext commandContext) {
        return executeQuery().size();
    }

    @Override
    public List<User> executeList(CommandContext commandContext) {
        return executeQuery();
    }

    protected List<User> executeQuery() {
        if (getId() != null) {
            List<User> result = new ArrayList<>();
            result.add(getById(getId()));
            return result;
        }
        else if (getIdIgnoreCase() != null) {
            List<User> result = new ArrayList<>();
            result.add(getById(getIdIgnoreCase()));
            return result;
        }
        else if (getFullNameLike() != null)
            return searchByNameIdQuery(getFullNameLike());
        else if (getFullNameLikeIgnoreCase() != null)
           return searchByNameIdQuery(getFullNameLikeIgnoreCase());
       else
           return executeAllUserQuery();

    }

    protected List<User> searchByNameIdQuery(String name) {
        String term = name.replaceAll("%", "");
        List<JsonNode> results = connectionUtil.queryUser(term);
        return results.stream().map(this::mapJsonResultToUser).collect(Collectors.toList());
    }

    protected List<User> executeAllUserQuery() {
        try {
            List<JsonNode> results = connectionUtil.getAllUsers();
            return results.stream().map(this::mapJsonResultToUser).collect(Collectors.toList());
        } catch (FlowableException fe) {
            LogUtils.logError(logger, "Unexpected error getting all users from repository: {}", fe, true, fe.getMessage());
            return Collections.emptyList();
        }
    }

    protected UserEntity getById(final String userId) {
        JsonNode user = connectionUtil.getUserById(userId);
        return mapJsonResultToUser(user);
    }

    private UserEntity mapJsonResultToUser(JsonNode entity) {
        if(entity.has("entry"))
            entity = entity.get("entry");
        UserEntity user = new UserEntityImpl();

        if (entity.has(UserProps.ID.label))
            user.setId(entity.get(UserProps.ID.label).asText());

        if (entity.has(UserProps.FIRST_NAME.label)) {
            try {
                user.setFirstName(entity.get(UserProps.FIRST_NAME.label).asText());
            } catch (NullPointerException e) {
                user.setFirstName("");
            }
        }
        if (entity.has(UserProps.LAST_NAME.label)) {
            try {
                user.setLastName(entity.get(UserProps.LAST_NAME.label).asText());
            } catch (NullPointerException e) {
                user.setLastName("");
            }
        }
        if (entity.has(UserProps.EMAIL.label)) {
            try {
                user.setEmail(entity.get(UserProps.EMAIL.label).asText());
            } catch (NullPointerException e) {
                user.setEmail("");
            }
        }
        return user;
    }

    public enum UserProps {
        ID("id"),
        AVATAR_ID("avatarId"),
        FIRST_NAME("firstName"),
        LAST_NAME("lastName"),
        EMAIL("email");

        public final String label;

        UserProps(String s) {
            this.label = s;
        }
    }
}
