package dk.magenta.flowable.idm.alfresco.engine;

import org.flowable.idm.engine.IdmEngineConfiguration;
import org.springframework.stereotype.Component;

@Component
public class AlfrescoEngineConfiguration extends IdmEngineConfiguration {

    public AlfrescoEngineConfiguration() {
        setUsingRelationalDatabase(false);
    }

    @Override
    public void initDataManagers() {
        // No need to initialize data managers when using alfresco
    }
}
