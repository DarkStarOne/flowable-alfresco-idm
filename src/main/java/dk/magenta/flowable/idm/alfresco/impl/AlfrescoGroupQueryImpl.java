/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.magenta.flowable.idm.alfresco.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil;
import dk.magenta.flowable.idm.alfresco.utils.Constants;
import dk.magenta.flowable.idm.alfresco.utils.LogUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.idm.api.Group;
import org.flowable.idm.engine.impl.GroupQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AlfrescoGroupQueryImpl extends GroupQueryImpl {
	private static final Logger logger = LoggerFactory.getLogger(AlfrescoGroupQueryImpl.class);
	private static final long serialVersionUID = -5362868729746803906L;

	private AlfrescoConnectionUtil connectionUtil;

	public AlfrescoGroupQueryImpl(AlfrescoConnectionUtil connectionUtil) {
		this.connectionUtil = connectionUtil;
	}

	@Override
	public long executeCount(CommandContext commandContext) {
		return executeQuery().size();
	}

	@Override
	public List<Group> executeList(CommandContext commandContext) {
		return executeQuery();
	}

	protected List<Group> executeQuery() {
		if (getUserId() != null)
			return findGroupsByUser(getUserId());
		else if (getId() != null)
			return findGroupsById(getId());
		else
			return findAllGroups();
	}

	/**
	 * Finds the groups a user belongs to
	 *
	 * @param userId
	 * @return
	 */
	protected List<Group> findGroupsByUser(String userId) {
		try {
			List<JsonNode> results = connectionUtil.getUserGroups(userId);
			return results.stream().map(this::mapJsonResultToGroup).collect(Collectors.toList());
		}
		catch (FlowableException fe) {
			LogUtils.logError(logger, "Unexpected error getting user's groups from repository: {}", fe, true, fe.getMessage());
		}
		return Collections.emptyList();
	}

	protected List<Group> findGroupsById(String term) {
		try {
			List<JsonNode> results = connectionUtil.queryGroups(term);
			return results.stream().map(this::mapJsonResultToGroup).collect(Collectors.toList());
		}
		catch (ConnectException ce){
            LogUtils.logError(logger, "Unable to query IDM repository for groups. Exception connecting to Alfresco.", ce, true);
        }
		catch (FlowableException fe) {
			LogUtils.logError(logger, "Unable to query repository for groups: {}", fe, true, fe.getMessage());
		}
		return Collections.emptyList();
	}

	protected List<Group> findAllGroups() {
		try {
			List<JsonNode> results = connectionUtil.getAllGroups();
			return results.stream().map(this::mapJsonResultToGroup).collect(Collectors.toList());
		}
		catch (FlowableException fe) {
			LogUtils.logError(logger, "Unexpected error getting all groups from repository: {}", fe, true, fe.getMessage());
			return Collections.emptyList();
		}
	}

	private GroupEntity mapJsonResultToGroup(JsonNode entity) {
		GroupEntity group = new GroupEntityImpl();
        if (entity.has(Constants.STR_ENTRY))
            entity = entity.get(Constants.STR_ENTRY);

		if (entity.has(GroupProps.ID.label)) {
			String groupId = entity.get(GroupProps.ID.label).asText();
            if (StringUtils.startsWith(groupId, AlfrescoConnectionUtil.GROUP_PREFIX))
                groupId = StringUtils.removeStart(groupId, AlfrescoConnectionUtil.GROUP_PREFIX);

			group.setId(groupId);
		}

		if (entity.has(GroupProps.DISPLAY_NAME.label)) {
			try {
				group.setName(entity.get(GroupProps.DISPLAY_NAME.label).asText());
			}
			catch (NullPointerException e) {
				group.setName("");
			}
		}
		return group;
	}

	public enum GroupProps {
		ID("id"),
		IS_ROOT("isRoot"),
		DISPLAY_NAME("displayName");

		public final String label;

		GroupProps(String s) {
			this.label = s;
		}
	}
}
