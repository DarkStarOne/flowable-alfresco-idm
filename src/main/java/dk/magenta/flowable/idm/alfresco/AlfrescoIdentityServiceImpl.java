/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.magenta.flowable.idm.alfresco;

import com.fasterxml.jackson.databind.JsonNode;
import dk.magenta.flowable.idm.alfresco.engine.AlfSession;
import dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil;
import dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil.GroupMembershipTypeQuery;
import dk.magenta.flowable.idm.alfresco.exception.AuthenticationException;
import dk.magenta.flowable.idm.alfresco.impl.AlfrescoGroupQueryImpl;
import dk.magenta.flowable.idm.alfresco.impl.AlfrescoUserQueryImpl;
import dk.magenta.flowable.idm.alfresco.utils.Constants;
import dk.magenta.flowable.idm.alfresco.utils.LogUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.idm.api.*;
import org.flowable.idm.engine.impl.IdmIdentityServiceImpl;
import org.flowable.idm.engine.impl.cmd.AddPrivilegeMappingCmd;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

public class AlfrescoIdentityServiceImpl extends IdmIdentityServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(AlfrescoIdentityServiceImpl.class);

    public static String ACCESS_ADMIN = "access-admin";

    private AlfrescoConnectionUtil connectionUtil;

    public AlfrescoIdentityServiceImpl(AlfrescoConnectionUtil alfrescoConnectionUtil) {
        this.connectionUtil = alfrescoConnectionUtil;
    }

    @Override
    public UserQuery createUserQuery() {
        return new AlfrescoUserQueryImpl(connectionUtil);
    }

    @Override
    public GroupQuery createGroupQuery() {
        return new AlfrescoGroupQueryImpl(connectionUtil);
    }

    /**
     * Returns all {@link Group} instances that have a particular privilege.
     */
    @Override
    public List<Group> getGroupsWithPrivilege(String name) {
        List<Group> groups = new ArrayList<>();
        if (!name.startsWith(AlfrescoConnectionUtil.PRIVILEGE_GROUP_PREFIX))
            name =  AlfrescoConnectionUtil.PRIVILEGE_GROUP_PREFIX + name;

        try {
            List<JsonNode> privilegedGroups = connectionUtil.getGroupMembers(name, GroupMembershipTypeQuery.GROUP);
            for (JsonNode member : privilegedGroups) {
                Group group = new GroupEntityImpl();
                String id = member.get(Constants.STR_ENTRY).get(Constants.STR_ID).asText();
                group.setId(id);
                group.setName(id);
                groups.add(group);
            }
            return groups;
        }
        catch (ConnectException ce) {
            LogUtils.logError(logger, "Exception connecting to alfresco repository.", ce, true);
            throw new FlowableException("Unable to connect to IDM repository.");
        }
    }

    /**
     * Returns all {@link User} instances that have a particular privilege.
     */
    @Override
    public List<User> getUsersWithPrivilege(String name) {
        List<User> users = new ArrayList<>();
        try {
            List<JsonNode> privilegedUsers = connectionUtil.getGroupMembers(AlfrescoConnectionUtil.PRIVILEGE_GROUP_PREFIX+name, GroupMembershipTypeQuery.PERSON);
            for (JsonNode member : privilegedUsers) {
                User user = new UserEntityImpl();
                user.setId(member.get(Constants.STR_ENTRY).get(Constants.STR_ID).asText());
                user.setFirstName(member.get(Constants.STR_ENTRY).get(Constants.STR_ID).asText());
                users.add(user);
            }
            return users;
        }
        catch (ConnectException ce) {
            LogUtils.logError(logger, "Exception connecting to alfresco repository.", ce, true);
            throw new FlowableException("Unable to connect to IDM repository.");
        }
    }

    @Override
    public void addUserPrivilegeMapping(String privilegeName, String userId) {
        String privilegeId = this.getPrivilegeByName(privilegeName).getId();
        List<PrivilegeMapping> privMap = this.getPrivilegeMappingsByPrivilegeId(privilegeId);
        for (PrivilegeMapping pm : privMap){
            if (null != pm.getUserId() && pm.getUserId().equals(userId))
                return;
        }
        List<User> usersWithPriv = this.getUsersWithPrivilege(privilegeName);
        for (User member : usersWithPriv) {
            if(member.getId().equals(userId)) {
                commandExecutor.execute(new AddPrivilegeMappingCmd(privilegeId, userId, null));
                break;
            }
        }
    }

    @Override
    public void addGroupPrivilegeMapping(String privilegeName, String groupId) {
        String privilegeId = this.getPrivilegeByName(privilegeName).getId();
        List<PrivilegeMapping> privMap = this.getPrivilegeMappingsByPrivilegeId(privilegeId);
        for (PrivilegeMapping pm : privMap)
            if (null != pm.getGroupId() && pm.getGroupId().equals(groupId))
                return;

        List<Group> groupsWithPriv = this.getGroupsWithPrivilege(privilegeName);
        for (Group member : groupsWithPriv) {
            if(member.getId().equals(groupId)){
                commandExecutor.execute(new AddPrivilegeMappingCmd(privilegeId, null, groupId));
                break;
            }
        }
    }

    @Override
    public boolean checkPassword(String userId, String password) {
        try {
            return executeCheckPassword(userId, password);
        }
        catch (ConnectException ce) {
            LogUtils.logError(logger, "Exception validating user authentication", ce, true);
            return false;
        }
    }

    protected boolean executeCheckPassword(final String userId, final String password) throws ConnectException {
        // Extra password check, see http://forums.activiti.org/comment/22312
        if (StringUtils.isAnyBlank(userId, password))
            throw new FlowableException("Null or empty \"passwords and/or user names\" are not allowed!");
        try {
            // It might be retrieved from cache; in which case the password/ticket will not be considered when
            // attempting to validate the session.
            AlfSession session = connectionUtil.getUserSession(userId, password);
            //In case the session is retrieved from cache and is an invalid session (Think logging out of alfresco)
            if(!connectionUtil.isValidSession(session)) {
                connectionUtil.removeSession(userId);
                // Create a new session object from the new token
                session = connectionUtil.getUserSession(userId, password);
            }
            return true;
        } catch (AuthenticationException | FlowableException e) {
            LogUtils.logInfoMsg(logger, "Could not verify authentication for user: {}", userId);
            return false;
        }
    }

    //<editor-fold desc="Some methods we need to override to do nothing">
    @Override
    public User newUser(String userId) {
        throw new FlowableException("Users should be created in your alfresco repository. This identity service doesn't support creating a new user");
    }

    @Override
    public void saveUser(User user) {
        throw new FlowableException("This identity service doesn't support saving a user");
    }

    @Override
    public void updateUserPassword(User user) {
        throw new FlowableException("Alfresco identity service does not support updating user password as of yet. " +
                "Please log in to your repository and change the password for the required user");
    }

    @Override
    public NativeUserQuery createNativeUserQuery() {
        throw new FlowableException("Alfresco identity service doesn't support native querying");
    }

    @Override
    public void deleteUser(String userId) {
        throw new FlowableException("Alfresco identity service doesn't support deleting an user.");
    }

    @Override
    public Group newGroup(String groupId) {
        throw new FlowableException("Alfresco identity service doesn't support creating a new group.");
    }

    @Override
    public NativeGroupQuery createNativeGroupQuery() {
        throw new FlowableException("Alfresco identity service doesn't support native querying.");
    }

    @Override
    public void saveGroup(Group group) {
        throw new FlowableException("Alfresco identity service doesn't support saving a group");
    }

    @Override
    public void deleteGroup(String groupId) {
        throw new FlowableException("Alfresco identity service doesn't support deleting a group.");
    }
    //</editor-fold>

    private Privilege getPrivilegeByName(String privilegeName){
        return this.createPrivilegeQuery().privilegeName(privilegeName).singleResult();
    }

}
