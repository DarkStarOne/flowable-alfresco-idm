package dk.magenta.flowable.idm.alfresco.engine;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import dk.magenta.flowable.idm.alfresco.AlfrescoIdentityServiceImpl;
import dk.magenta.flowable.idm.alfresco.exception.AuthenticationException;
import dk.magenta.flowable.idm.alfresco.impl.AlfrescoGroupQueryImpl;
import dk.magenta.flowable.idm.alfresco.utils.Constants;
import dk.magenta.flowable.idm.alfresco.utils.LogUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.core.util.IOUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.idm.api.IdmIdentityService;
import org.flowable.idm.api.Privilege;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for managing Alfresco connection.
 *
 * @author DarkStar1
 */
public class AlfrescoConnectionUtil {
	private static final Logger logger = LoggerFactory.getLogger(AlfrescoConnectionUtil.class);

	/* Server connection params */
	private int port;
	private String protocolType;
	private String serverUrl;
	private String serviceUserName;
	private String serviceUserPassword;

	private static final XmlMapper xmlMapper = new XmlMapper();
	private static final ObjectMapper objectMapper = new ObjectMapper();

	// Connect timeout is five seconds so that we fail fast if the server, Alfresco, is down.
	private static final int CONNECT_TIMEOUT_MILLIS = 5_000;

	//Read timeout is 5 minutes so that Alfresco may have time to respond.
	private static final int READ_TIMEOUT_MILLIS = 300_000;
	public static final String AUTH_HEADER_PREFIX = "Basic ";
	public static String JSESSION_COOKIE_ID = "JSESSIONID";
	public static String GROUP_PREFIX = "GROUP_";
	public static String PRIVILEGE_GROUP_PREFIX = "GROUP_FLOWABLE_";
	public final String PUBLIC_API_BASE_PATH = "/alfresco/api/-default-/public";
	public final String TICKET_PREFIX = "TICKET_";

	//Constructor
	public AlfrescoConnectionUtil() {}

	/**
	 * Authenticates a user against the configured alfresco repository and creates an {@link AlfSession} object if
	 * successful.
	 *
	 * @param userName
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@Cacheable(value = "sessionsCache", key = "#userName")
	public AlfSession getUserSession(String userName, String password) throws AuthenticationException, ConnectException {
		try {
			// First check if we've been given a ticket instead of an actual password, validate it and then return that
			// ticket in a session object instead
			Pair<Boolean, AlfSession> ticketTest = isTicketAuth(userName, password);
			if (ticketTest.getLeft())
				return ticketTest.getRight();

			//if not assume regular password.
			String loginStr = "{ \"userId\" : \"" + userName + "\", \"password\" : \"" + password + "\" }";
			String url = getAPIUrlString(ApiPath.LOGIN);
			Pair<Integer, JsonNode> response = makeConnection(url, ConnectionVerb.POST, null, objectMapper.readTree(loginStr));

			if (response.getLeft() != HttpURLConnection.HTTP_OK && response.getLeft() != HttpURLConnection.HTTP_CREATED) {
				throw new ConnectException("Unable to authenticate user against Alfresco repository");
			}
			return new AlfSession(response.getRight().get(Constants.STR_ENTRY).get(Constants.STR_ID).toString(), userName);
		}
		catch (ConnectException ce) {
			LogUtils.logError(logger, "Exception contacting Alfresco repository. Unable to authenticate user", ce, true);
			throw ce;
		}
		catch (Exception ge) {
			LogUtils.logError(logger, "Exception authenticating user: {}", ge, true, userName);
			throw new AuthenticationException("Error when attempting to authenticate user: " + ge.getMessage());
		}
	}

	/**
	 * Checks if a session object is still valid. The check is done against the ticket.
	 *
	 * @param session
	 * @return
	 */
	public boolean isValidSession(AlfSession session) throws ConnectException {
		try {
			String url = getAPIUrlString(ApiPath.LOGIN) + "/-me-";
			Pair<Integer, JsonNode> response = makeConnection(url, ConnectionVerb.GET, session, null);
			String responseTicket = response.getRight().get(Constants.STR_ENTRY).get(Constants.STR_ID).asText();
			return (response.getLeft() == HttpURLConnection.HTTP_OK && responseTicket.equals(session.getTicket()));
		}
		catch (ConnectException ce) {
			LogUtils.logError(logger, "Exception contacting Alfresco repository. Unable to validate user session ticket", ce, true);
			throw ce;
		}
	}

	@CacheEvict(cacheNames = "sessionsCache", key = "#userId")
	public void removeSession(String userId) {}

	/**
	 * Returns the URL required to access the given REST api.
	 * @param apiPath - currently only supports {GROUP | LOGIN | PEOPLE | PEOPLE_QUERY | SEARCH}
	 * @return
	 */
	public final String getAPIUrlString(ApiPath apiPath) {
		if (port != 80 && port != 443)
			return this.protocolType + "://" + serverUrl + ":" + port + PUBLIC_API_BASE_PATH + apiPath.label;
		else
			return this.protocolType + "://" + serverUrl + PUBLIC_API_BASE_PATH + apiPath.label;
	}

	//Methods to support User queries
	public List<JsonNode> getAllUsers() throws FlowableException {
		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String peopleAPIPath = getAPIUrlString(ApiPath.PEOPLE);
			Pair<Integer, JsonNode> response = makeConnection(peopleAPIPath, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to retrieve the list of all users from the repository");

			return getQueryResponseList(response.getRight().get("list"));
		}
		catch (Exception ge) {
			logger.error("Error getting all users from alfresco repository: " + ge.getMessage());
			ge.printStackTrace();
			throw new FlowableException("Exception retrieving all users from identity store ");
		}
	}

	/**
	 * Get a single user from the configured alfresco repository.
	 *
	 * @param userId
	 * @return
	 */
	public JsonNode getUserById(String userId) {
		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String peopleAPIPath = getAPIUrlString(ApiPath.PEOPLE) + "/" + userId;
			Pair<Integer, JsonNode> response = makeConnection(peopleAPIPath, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to retrieve the user from the repository");

			logger.info("Successfully retrieved user (" + userId + ") from repository");
			return response.getRight().get(Constants.STR_ENTRY);
		}
		catch (Exception ge) {
			LogUtils.logError(logger,"Error getting user [{}] from alfresco repository: {}",ge, true,userId, ge.getMessage());
			throw new FlowableException("Exception retrieving user from identity store ");
		}
	}

	/**
	 * Get a single group from the configured alfresco repository.
	 *
	 * @param groupId
	 * @return
	 */
	public JsonNode getGroupById(String groupId) {
		try {
			//First check if it starts with the GROUP_ prefix because alfresco requires this to be the case
			if (!groupId.startsWith(GROUP_PREFIX))
				groupId = GROUP_PREFIX + groupId;

			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String groupAPIPath = getAPIUrlString(ApiPath.GROUP) + "/" + groupId;
			Pair<Integer, JsonNode> response = makeConnection(groupAPIPath, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to retrieve the group from the repository");

			LogUtils.logInfoMsg(logger, "Successfully retrieved group ({}) from repository", groupId);
			return response.getRight().get(Constants.STR_ENTRY);
		}
		catch (Exception ge) {
			LogUtils.logError(logger, "Error getting group [{}] from alfresco repository: {} ", ge, true, groupId, ge.getMessage());
			throw new FlowableException("Exception retrieving group from identity store ");
		}
	}

	/**
	 * Query for users
	 *
	 * @param term
	 * @return
	 */
	public List<JsonNode> queryUser(String term) {
		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String peopleQueryAPI = getAPIUrlString(ApiPath.PEOPLE_QUERY) + "?term=" + term +
					"&skipCount=0&maxItems=100&fields=email%2C%20id%2C%20firstName%2C%20lastName";
			Pair<Integer, JsonNode> response = makeConnection(peopleQueryAPI, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to query for users using: " + term + ", from the repository");

			return getQueryResponseList(response.getRight().get("list"));
		}
		catch (ConnectException se) {
			LogUtils.logError(logger,"Error getting users matching [{}] from alfresco repository: {}", se, true, term, se.getMessage());
			throw new FlowableException("Exception querying users from identity store ");
		}
	}

	/**
	 * Retrieves a user's groups from Alfresco
	 *
	 * @param userId
	 * @return
	 */
	@Cacheable(value = "usersGroupCache", key = "#userId")
	public List<JsonNode> getUserGroups(String userId) {
		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String peopleAPIPath = getAPIUrlString(ApiPath.PEOPLE) + "/" + userId + "/groups";
			Pair<Integer, JsonNode> response = makeConnection(peopleAPIPath, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK) {
				throw new ConnectException("Unable to retrieve the list of " + userId + "'s groups from the repository");
			}
			return getQueryResponseList(response.getRight().get("list"));
		}
		catch (Exception ge) {
			LogUtils.logError(logger,"Error getting user [{}'s] groups from alfresco repository: {}", ge, true, userId, ge.getMessage());
			throw new FlowableException("Exception retrieving user groups identity store ");
		}
	}

	/**
	 * Retrieve all groups from the alfresco repository
	 *
	 * @return
	 * @throws FlowableException
	 */
	public List<JsonNode> getAllGroups() throws FlowableException {
		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String groupAPIPath = getAPIUrlString(ApiPath.GROUP);
			Pair<Integer, JsonNode> response = makeConnection(groupAPIPath, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to retrieve the list of all groups from the repository");

			return getQueryResponseList(response.getRight().get("list"));
		}
		catch (Exception ge) {
			LogUtils.logError(logger, "Error getting all groups from alfresco repository: {}", ge, true, ge.getMessage());
			throw new FlowableException("Exception retrieving all users from identity store ");
		}
	}

	/**
	 * Querying groups from the repository
	 *
	 * @param term
	 * @return
	 */
	public List<JsonNode> queryGroups(String term) throws ConnectException {
		try {
			JsonNode payload = objectMapper.readTree("{ \"query\": {" +
					"\"query\": \"TYPE:\\\"cm:authorityContainer\\\" AND cm:authorityName:\\\"*" + term + "*\\\"" +
					"\"}, \"paging\": { \"maxItems\": 100, \"skipCount\": 0 }, \"include\": [\"properties\"] }");
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String searchQueryPath = getAPIUrlString(ApiPath.SEARCH);
			Pair<Integer, JsonNode> response = makeConnection(searchQueryPath, ConnectionVerb.POST, serviceUserSession, payload);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to query groups using the term [" + term + "], from the repository.");

			return getQueryResponseList(response.getRight().get("list"));
		}
		catch (ConnectException ce) {
			LogUtils.logError(logger, "Exception contacting Alfresco repository. Unable to initialise privileges.", ce, true);
			throw ce;
		}
		catch (IOException ioe) {
			LogUtils.logError(logger, "Error querying groups from alfresco repository: {}", ioe, true, ioe.getMessage());
			throw new FlowableException("Exception querying groups from identity store.");
		}
	}

	@Cacheable(value = "groupMembersCache", key = "#groupId")
	public List<JsonNode> getGroupMembers(String groupId, GroupMembershipTypeQuery memberType) throws ConnectException {
		if (!groupId.startsWith(GROUP_PREFIX))
			groupId = GROUP_PREFIX + groupId;

		try {
			AlfSession serviceUserSession = getUserSession(serviceUserName, serviceUserPassword);
			String membershipQueryString = getAPIUrlString(ApiPath.GROUP);
			StringBuilder builder = new StringBuilder().append(membershipQueryString).append("/");
			builder.append(groupId).append("/").append(Constants.STR_MEMBERS).append("?").append(memberType.label);
			String url = builder.toString();
			Pair<Integer, JsonNode> response = makeConnection(url, ConnectionVerb.GET, serviceUserSession, null);
			if (response.getLeft() != HttpURLConnection.HTTP_OK)
				throw new ConnectException("Unable to query for members of group [" + groupId + "], from the repository.");

			return getQueryResponseList(response.getRight().get(Constants.STR_LIST));
		}
		catch (ConnectException ce) {
			LogUtils.logError(logger, "Exception contacting Alfresco repository. Unable to initialise privileges.", ce, true);
			throw ce;
		}
		catch (Exception ge) {
			LogUtils.logError(logger, "Error querying group membership from alfresco repository: {}", ge, true, ge.getMessage());
			throw new FlowableException("Exception querying group memberships from identity store.");
		}
	}

	/**
	 * Will get groups that are used in the role of emulating Flowable privileges
	 *
	 * @return
	 */
	public List<JsonNode> getFlowablePrivilegeGroups() throws ConnectException {
		return this.queryGroups(PRIVILEGE_GROUP_PREFIX);
	}

	@Override
	public String toString() {
		return "AlfrescoConnectionUtil{" +
				"port=" + port +
				", serverUrl='" + serverUrl + '\'' +
				", serviceUserName='" + serviceUserName + '\'' +
				", connectionProtocolType='" + protocolType + '\'' +
				'}';
	}

	public enum ConnectionVerb {
		GET("GET"),
		POST("POST"),
		PUT("PUT"),
		DELETE("DELETE");

		public final String label;

		ConnectionVerb(String s) {
			this.label = s;
		}
	}

	protected enum ApiPath {
		GROUP("/alfresco/versions/1/groups"),
		LOGIN("/authentication/versions/1/tickets"),
		PEOPLE("/alfresco/versions/1/people"),
		SEARCH("/search/versions/1/search"),
		PEOPLE_QUERY("/alfresco/versions/1/queries/people");

		public final String label;

		ApiPath(String s) {
			this.label = s;
		}
	}

	public enum GroupMembershipTypeQuery {
		GROUP("where=(memberType%3D'GROUP')"),
		PERSON("where=(memberType%3D'PERSON')"),
		BOTH("");
		public final String label;

		GroupMembershipTypeQuery(String s) {
			this.label = s;
		}

	}

	//<editor-fold desc="Property getters and setters">
	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getServiceUserName() {
		return serviceUserName;
	}

	public void setServiceUserName(String user) {
		this.serviceUserName = user;
	}

	public String getServiceUserPassword() {
		return serviceUserPassword;
	}

	public void setServiceUserPassword(String serviceUserPassword) {
		this.serviceUserPassword = serviceUserPassword;
	}

	public String getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}
	//</editor-fold>

	/**
	 * Initialise privileges from Alfresco
	 */
	public void initPrivileges(IdmIdentityService identityService) {
		LogUtils.logInfoMsg(logger, "Initialising privileges from Alfresco repository.............");
		try {
			// The list of Flowable privilege groups defined in Alfresco
			List<JsonNode> results = this.getFlowablePrivilegeGroups();
			// The list of privileges that might have been created in Flowable by this point
			List<String> flowablePrivileges = isPrivilegeMappingsCreated(identityService).getRight();
			List<String> groupIds = results.stream().map(this::getGroupId).collect(Collectors.toList());

			// First determine if any privileges have been created in Flowable
			for (String alfPrivilegeGroup : groupIds) {
				String alfPrivilegeName = StringUtils.removeStart(alfPrivilegeGroup, PRIVILEGE_GROUP_PREFIX);
				//Create the privileges that are not already created
				if (!flowablePrivileges.contains(alfPrivilegeName)) {
					identityService.createPrivilege(alfPrivilegeName);
				}
				//Create the privilege mappings for the users as defined in the Alfresco Flowable privilege groups
				List<JsonNode> members = this.getGroupMembers(alfPrivilegeGroup, GroupMembershipTypeQuery.BOTH);
				for (JsonNode member : members) {
					String ID = member.get(Constants.STR_ENTRY).get(Constants.STR_ID).asText();
					String memberType = member.get(Constants.STR_ENTRY).get(Constants.STR_MEMBER_TYPE).asText();
					if (memberType.equalsIgnoreCase(Constants.STR_PERSON)) {
						identityService.addUserPrivilegeMapping(alfPrivilegeName, ID);
					}
					if (memberType.equalsIgnoreCase(Constants.STR_GROUP)) {
						identityService.addGroupPrivilegeMapping(alfPrivilegeName, ID);
					}
				}
			}
			LogUtils.logInfoMsg(logger, "Privileges initialised.");
		}

		catch (ConnectException | ResponseStatusException ge) {
			LogUtils.logError(logger, "Exception contacting Alfresco repository. Unable to initialise privileges.", ge, true);
			LogUtils.logInfoMsg(logger, "Alfresco Service unreachable.");
			if(identityService.createPrivilegeQuery().privilegeName(AlfrescoIdentityServiceImpl.ACCESS_ADMIN).singleResult() == null)
				identityService.createPrivilege(AlfrescoIdentityServiceImpl.ACCESS_ADMIN);
			else
				throw new FlowableException("Unable to reach Alfresco IDM.");

		}
	}

	/**
	 * Private methods
	 */

	/**
	 * Makes a connection to the alfresco server and then returns the response
	 *
	 * @param urlString
	 * @param verb
	 * @param session
	 * @param payload
	 * @return a Pair<ConnectionStatus, response>
	 * @throws ConnectException
	 */
	private Pair<Integer, JsonNode> makeConnection(String urlString, ConnectionVerb verb, @Nullable AlfSession session,
												   @Nullable JsonNode payload) throws ConnectException, HttpStatusCodeException {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urlString);
			if (url.getProtocol().equals(Constants.STR_HTTPS))
				conn = (HttpsURLConnection) url.openConnection();
			else
				conn = (HttpURLConnection) url.openConnection();

			conn.setRequestMethod(verb.label);
			conn.setRequestProperty(Constants.HTTP_HEADERS.CONTENT_TYPE, Constants.MEDIA_MIME_TYPES.APPLICATION_JSON);
			conn.setRequestProperty(Constants.HTTP_HEADERS.CONTENT_LANGUAGE, "en-GB");
			conn.setRequestProperty(Constants.HTTP_HEADERS.ACCEPT, Constants.MEDIA_MIME_TYPES.APPLICATION_JSON);
			conn.setConnectTimeout(CONNECT_TIMEOUT_MILLIS);
			conn.setReadTimeout(READ_TIMEOUT_MILLIS);

			if (session != null)
				conn.setRequestProperty(Constants.HTTP_HEADERS.AUTHORIZATION, base64EncodeTicket(session));

			// If post or put verbs, then a payload is assumed to be present. So send payload.
			if (payload != null && (verb.label.equalsIgnoreCase(ConnectionVerb.POST.label) || verb.label.equalsIgnoreCase(ConnectionVerb.PUT.label))) {
				conn.setDoOutput(true);
				try (OutputStream os = conn.getOutputStream()) {
					byte[] input = objectMapper.writeValueAsString(payload).getBytes(StandardCharsets.UTF_8.toString());
					os.write(input, 0, input.length);
				}
				catch (Exception ge) {
					throw new ConnectException("An issue sending payload to the the alfresco server: " + ge.getMessage());
				}
			}

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK || conn.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
				try (InputStream is = conn.getInputStream()) {
					String contentType = conn.getHeaderField(Constants.HTTP_HEADERS.CONTENT_TYPE);
					switch (contentType) {
						case Constants.MEDIA_MIME_TYPES.TEXT_XML:
							return new ImmutablePair<>(conn.getResponseCode(), xmlMapper.readTree(is));
						case Constants.MEDIA_MIME_TYPES.APPLICATION_JSON:
						default:
							return new ImmutablePair<>(conn.getResponseCode(), objectMapper.readTree(is));
					}
				}
				catch (IOException ioe) {
					LogUtils.logError(logger, "Error encounter whilst attempting to read server response: {}", ioe, true, ioe.getMessage());
					throw new ConnectException("An issue reading response from server: " + ioe.getMessage());
				}
			}
			else {
				InputStreamReader reader = new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8);
				String responseBody = IOUtils.toString(reader);
				reader.close();
				throw new ResponseStatusException(HttpStatus.valueOf(conn.getResponseCode()), responseBody);
			}
		}
		catch (IOException ioe) {
			if (ioe.getClass() == SocketTimeoutException.class) {
				LogUtils.logWarning(logger, "Connection timeout when attempting to contact Alfresco server.");
				throw new ConnectException("Unable to contact Alfresco server due to connection timeout");
			}
			else {
				throw new ConnectException("Unforeseen exception connecting or reading response from the Alfresco server:" + ioe.getMessage());
			}
		}
		finally {
			if (conn != null)
				conn.disconnect();
		}
	}

	/**
	 * Extracts the list of JSON objects from the response object of a query.
	 *
	 * @param responseObject
	 * @return
	 */
	private List<JsonNode> getQueryResponseList(JsonNode responseObject) {
		JsonNode entries = responseObject.get(Constants.STR_ENTRIES);
		List<JsonNode> items = new ArrayList<>();
		if (entries.isArray())
			entries.elements().forEachRemaining(items::add);

		if (items.size() > 0)
			return items;
		else
			return Collections.emptyList();
	}

	/**
	 * Used to encode the ticket for the header.
	 */
	private String base64EncodeTicket(AlfSession session) {
		String encodedTicket = Base64.getEncoder().encodeToString(session.getTicket().getBytes());
		return AUTH_HEADER_PREFIX + encodedTicket;
	}

	/**
	 * This method tries to assume that the password is an actual alfresco ticket and tries to validate the ticket
	 * against alfresco under that assumption.
	 *
	 * @param userName
	 * @param ticket
	 * @return
	 */
	private Pair<Boolean, AlfSession> isTicketAuth(String userName, String ticket) {
		AlfSession testedSession;
		try {
			if (ticket.startsWith(TICKET_PREFIX)) {
				testedSession = new AlfSession(ticket, userName);
				return new ImmutablePair<>(true, testedSession);
			}

			//If the next line throws an error then it is not valid anyways
			String decodedTicket = new String(Base64.getDecoder().decode(ticket.getBytes()));

			if (decodedTicket.startsWith(TICKET_PREFIX)) {
				testedSession = new AlfSession(decodedTicket, userName);
				return new ImmutablePair<>(true, testedSession);
			}
		}
		catch (IllegalArgumentException iae) {
			LogUtils.logWarning(logger, "Error decoding ticket. Perhaps ticket was not a ticket.");
			return new ImmutablePair<>(false, null);
		}
		return new ImmutablePair<>(false, null);
	}

	private String getGroupId(JsonNode entity) {
		if (entity.has(Constants.STR_ENTRY))
			entity = entity.get(Constants.STR_ENTRY);

		if (entity.has(AlfrescoGroupQueryImpl.GroupProps.ID.label)) {
			String groupId = entity.get(AlfrescoGroupQueryImpl.GroupProps.ID.label).asText();
			if (StringUtils.startsWith(groupId, AlfrescoConnectionUtil.PRIVILEGE_GROUP_PREFIX))
				return groupId;
		}
		return "";
	}

	/**
	 * Checks if Flowable has created privilege mappings
	 *
	 * @return The evaluated boolean condition and a list of the mappings if true or empty list if false.
	 */
	private Pair<Boolean, List<String>> isPrivilegeMappingsCreated(IdmIdentityService identityService) {
		List<String> flowablePrivileges = identityService.createPrivilegeQuery().list().stream().map(Privilege::getName).collect(Collectors.toList());
		if (flowablePrivileges.size() > 0)
			return new ImmutablePair<>(true, flowablePrivileges);
		else
			return new ImmutablePair<>(false, Collections.emptyList());
	}
}
