package dk.magenta.flowable.idm.alfresco.config;

import dk.magenta.flowable.idm.alfresco.AlfrescoIdentityServiceImpl;
import dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil;
import org.flowable.idm.api.IdmIdentityService;
import org.flowable.idm.spring.SpringIdmEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.flowable.spring.boot.ProcessEngineAutoConfiguration;
import org.flowable.spring.boot.ProcessEngineServicesAutoConfiguration;
import org.flowable.spring.boot.app.AppEngineAutoConfiguration;
import org.flowable.spring.boot.app.AppEngineServicesAutoConfiguration;
import org.flowable.spring.boot.condition.ConditionalOnIdmEngine;
import org.flowable.spring.boot.idm.IdmEngineAutoConfiguration;
import org.flowable.spring.security.FlowableAuthenticationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.List;

/**
 * Inspired by a combination of:
 * 1 - {@link IdmEngineAutoConfiguration}
 * 2 - {@link FlowableLdapAutoConfiguration } - https://github.com/flowable/flowable-engine/blob/flowable-6.4.1/modules/flowable-spring-boot/flowable-spring-boot-starters/flowable-spring-boot-autoconfigure/src/main/java/org/flowable/spring/boot/ldap/FlowableLdapAutoConfiguration.java
 * See also the following forum post: https://forum.flowable.org/t/idm-service-and-management-custom-implmentation/4280/2?u=darkstar1
 * as well as the docs on engine configuration.
 * @author DarkStar1
 */
@Configuration
@ConditionalOnIdmEngine
@ConditionalOnProperty(prefix = "flowable.idm.alfresco", name = "enabled", havingValue = "true", matchIfMissing = false)
@AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE)
@AutoConfigureAfter({AppEngineAutoConfiguration.class, ProcessEngineAutoConfiguration.class,})
@AutoConfigureBefore({IdmEngineAutoConfiguration.class, AppEngineServicesAutoConfiguration.class, ProcessEngineServicesAutoConfiguration.class})
public class FlowableConfiguration {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    @ConditionalOnMissingBean(type = {"dk.magenta.flowable.idm.alfresco.engine.AlfrescoConnectionUtil"})
    @ConfigurationProperties(prefix = "flowable.idm.alfresco")
    public AlfrescoConnectionUtil setConnectionUtil() {
        System.out.println();
        logger.info("\nInitialising Alfresco connection parameters for IDM...\n");
        System.out.println();
        return new AlfrescoConnectionUtil();
    }

    @Bean
    public EngineConfigurationConfigurer<SpringIdmEngineConfiguration> alfrescoIdmEngineConfigurer(AlfrescoConnectionUtil connectionUtil) {
        return idmEngineConfiguration -> idmEngineConfiguration
                .setIdmIdentityService(new AlfrescoIdentityServiceImpl(connectionUtil));
    }

    // We need a custom AuthenticationProvider for the Alfresco Support
    // since the default (DaoAuthenticationProvider) from Spring Security
    // uses a Password encoder to perform the matching and we need to use
    // the IdmIdentityService for Alfresco
    @Bean
    @ConditionalOnMissingBean(AuthenticationProvider.class)
    public FlowableAuthenticationProvider flowableAuthenticationProvider(IdmIdentityService idmIdentitySerivce,
                                                                         UserDetailsService userDetailsService) {
        return new FlowableAuthenticationProvider(idmIdentitySerivce, userDetailsService);
    }

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        List<Cache> caches = new ArrayList<>(2);
        caches.add(new ConcurrentMapCache("groupMembersCache"));
        caches.add(new ConcurrentMapCache("sessionsCache"));
        caches.add(new ConcurrentMapCache("usersGroupCache"));
        simpleCacheManager.setCaches(caches);

        return simpleCacheManager;
    }

    @Bean
    public ApplicationListener<ContextRefreshedEvent> startupPrivilegesInitializer(AlfrescoConnectionUtil connectionUtil, IdmIdentityService idmIdentityService) {
        return args -> {
            connectionUtil.initPrivileges(idmIdentityService);
        };
    }

}
