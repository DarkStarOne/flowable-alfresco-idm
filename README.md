# Flowable Alfresco IDM module
===============================

This module allows a [Flowable][1] workflow engine to use Alfresco as its identity manager (IDM).
<br/>This module was built and tested against a Flowable springboot application and as such, hasn't been tested against an 
out-of-the-box Flowable application.

#####Note:
As long as this is still here, the user should take this project as a work in progress and as such not yet complete; For
instance, **_this module does not yet support the Flowable modeler_** at the time of writing.

Installation/Use
----------------
There are 2 additional directories in the project that correspond to the components related to using this module,
the [Alfresco][2] folder and the [Flowable Springboot App] folder:
1. The Alfresco Folder

   This contains bootstrapping configuration for the five Flowable privilege groups. The files have been placed in a 
   path similar to where you will need to place this within an Alfresco developer's SDK 4 maven project.
   Note that this is optional as you can choose to ignore this but you will have to create the groups in Alfresco if you 
   choose to do so. <br/>
   If you want to create other privileges groups and have them used in Flowable, this is also possible.
   The naming of the groups should follow the pattern GROUP_FLOWABLE_my-group-name where in the prefix **GROUP_FLOWABLE_**
   is mandatory to the naming of the group(s).
   
2. The Flowable Springboot App folder

      This contains instances of [WebSecurityConfigurerAdapter][3] and was added to get around security issues with
      using the REST api to interact with the process engine.<br/>
      All that is needed is to copy this into your springboot application, and set the property
      `spring.profiles.active=[dev | production]` in your application.[properties | yml] config file.
      
   
Other than this, you simply need to build and install this project in a maven repository (`mvn clean package install`) 
then add it as a dependency to your project.



[1]: https://www.flowable.org/
[2]: https://www.alfresco.com/
[3]: https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/configuration/WebSecurityConfigurerAdapter.html
